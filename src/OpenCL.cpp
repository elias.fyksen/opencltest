//
// Created by elias on 1/14/18.
//

#include <iostream>
#include <csetjmp>
#include "OpenCL.h"



OpenCL::OpenCL() {
    initPlatforms();
}

OpenCL::~OpenCL() {

    for(size_t i = 0; i < platformArraySize; i++){

        for(size_t j = 0; j < platformArray[i].deviceArraySize; j++){
            delete platformArray[i].deviceArray[j].name;
            delete platformArray[i].deviceArray[j].vendor;
        }

        delete platformArray[i].deviceArray;
    }
}

void OpenCL::initPlatforms() {

    //Getting platform information
    std::cout << std::endl;

    clGetPlatformIDs(0, nullptr, &platformArraySize); //Getting amount of platfomrs

    auto *platformIds = new cl_platform_id[platformArraySize];
    platformArray = new Platform[platformArraySize];

    if(clGetPlatformIDs(platformArraySize, platformIds, &platformArraySize) == CL_SUCCESS)
        std::cout << "Successfully got " << platformArraySize << " platform id(s)" << std::endl;
    else{
        std::cout << "ERROR: Failed to get platform ids!" << std::endl;
        exit(101);
    }

    //Formatting text nicely
    std::cout << std::endl;
    std::cout << "Platform info:" << std::endl;

    for(size_t i = 0; i < platformArraySize; i++){
        std::cout << std::endl;

        std::cout << "PLATFORM " << i << ":" << std::endl;

        Platform &platform = platformArray[i];

        platform.id = platformIds[i];

        size_t length;
        cl_int result;


        //Getting platform name

        result = clGetPlatformInfo(platform.id, CL_PLATFORM_NAME, 0, nullptr, &length);

        if(result != CL_SUCCESS)
            if(result == CL_INVALID_PLATFORM)
                std::cout << std::endl << "ERROR: INVALID PLATFORM ID!";

            else if(result == CL_INVALID_VALUE)
                std::cout << std::endl << "ERROR: INVALID PLATFORM VALUE NAME!";

            else
                std::cout << std::endl << "ERROR: UNKNOWN ERROR WHILE GETTING PLATFORM NAME";

        platform.name = new char[length];
        result = clGetPlatformInfo(platform.id, CL_PLATFORM_NAME, length, platform.name, nullptr);

        if(result != CL_SUCCESS)
            std::cout << std::endl << "ERROR: UNKNOWN ERROR WHILE GETTING PLATFORM NAME";

        std::cout << "\tNAME:\t\t" << platform.name << std::endl;


        //Getting platform vendor

        result = clGetPlatformInfo(platform.id, CL_PLATFORM_VENDOR, 0, nullptr, &length);

        if(result != CL_SUCCESS)
            if(result == CL_INVALID_PLATFORM)
                std::cout << std::endl << "ERROR: INVALID PLATFORM ID!";

            else if(result == CL_INVALID_VALUE)
                std::cout << std::endl << "ERROR: INVALID PLATFORM VALUE NAME!";

            else
                std::cout << std::endl << "ERROR: UNKNOWN ERROR WHILE GETTING PLATFORM VENDOR";

        platform.vendor = new char[length];
        result = clGetPlatformInfo(platform.id, CL_PLATFORM_VENDOR, length, platform.vendor, nullptr);

        if(result != CL_SUCCESS)
            std::cout << std::endl << "ERROR: UNKNOWN ERROR WHILE GETTING PLATFORM VENDOR";

        std::cout << "\tVENDOR:\t\t" << platform.vendor << std::endl;

        platform.initDevices();
    }

    std::cout << std::endl;
}

void OpenCL::Platform::initDevices() {

    cl_int result;

    result = clGetDeviceIDs(id, CL_DEVICE_TYPE_ALL, 0, nullptr, &deviceArraySize);

    if(result == CL_SUCCESS)
        std::cout << "\tD. FOUND:\t" << deviceArraySize << std::endl;

    else if(result == CL_DEVICE_NOT_FOUND) {
        std::cout << "\tERROR: No devices found";
        return;

    } else if(result == CL_INVALID_PLATFORM){
        std::cout << std::endl << "\tERROR: INVALID PLATFORM ID!";
        return;
    }

    auto *clDeviceIds = new cl_device_id[deviceArraySize];
    deviceArray = new Device[deviceArraySize];

    result = clGetDeviceIDs(id, CL_DEVICE_TYPE_ALL, deviceArraySize, clDeviceIds, nullptr);

    if(result != CL_SUCCESS){
        std::cout << "\tERROR: Unknown error while getting device ids" << std::endl;
        return;
    }

    for(size_t i = 0; i < deviceArraySize; i++){

        Device &device = deviceArray[i];

        device.id = clDeviceIds[i];

        std::cout << std::endl;
        std::cout << "\tDEVICE " << i << ':' << std::endl;

        size_t length;


        //Getting device name

        result = clGetDeviceInfo(device.id, CL_DEVICE_NAME, 0, nullptr, &length);

        if(result != CL_SUCCESS)
            if(result == CL_INVALID_DEVICE)
                std::cout << std::endl << "\t\tERROR: Invalid device id!";

            else if(result == CL_INVALID_VALUE)
                std::cout << std::endl << "\t\tERROR: INVALID DEVICE VALUE NAME!";

            else
                std::cout << std::endl << "\t\tERROR: UNKNOWN ERROR WHILE GETTING DEVICE NAME";

        device.name = new char[length];
        result = clGetDeviceInfo(device.id, CL_DEVICE_NAME, length, device.name, nullptr);

        if(result != CL_SUCCESS)
            std::cout << std::endl << "\t\tERROR: UNKNOWN ERROR WHILE GETTING DEVICE NAME";

        std::cout << "\t\tNAME:\t\t" << device.name << std::endl;


        //Getting device vendor

        result = clGetDeviceInfo(device.id, CL_DEVICE_VENDOR, 0, nullptr, &length);

        if(result != CL_SUCCESS)
            if(result == CL_INVALID_PLATFORM)
                std::cout << std::endl << "\t\tERROR: INVALID DEVICE ID!";

            else if(result == CL_INVALID_VALUE)
                std::cout << std::endl << "\t\tERROR: INVALID DEVICE VALUE NAME!";

            else
                std::cout << std::endl << "\t\tERROR: UNKNOWN ERROR WHILE GETTING DEVICE VENDOR";

        device.vendor = new char[length];
        result = clGetDeviceInfo(device.id, CL_DEVICE_VENDOR, length, device.vendor, nullptr);

        if(result != CL_SUCCESS)
            std::cout << std::endl << "\t\tERROR: UNKNOWN ERROR WHILE GETTING DEVICE VENDOR";

        std::cout << "\t\tVENDOR:\t\t" << device.vendor << std::endl;


        result = clGetDeviceInfo(device.id, CL_DEVICE_TYPE, 8, &device.type, nullptr);

        if(result == CL_SUCCESS);
        else if(result == CL_INVALID_DEVICE)
            std::cout << "\t\tERROR: Invalid device id" << std::endl;
        else if(result == CL_INVALID_VALUE)
            std::cout << "\t\tERROR: Invalid device value name" << std::endl;
        else
            std::cout << "\t\tERROR: Unknown error while getting device type" << std::endl;

        std::cout << "\t\tTYPE:\t\t";
        switch (device.type){
            case CL_DEVICE_TYPE_CPU:
                std::cout << "CPU";
                break;
            case CL_DEVICE_TYPE_ACCELERATOR:
                std::cout << "ACCELERATOR";
                break;
            case CL_DEVICE_TYPE_CUSTOM:
                std::cout << "CUSTOM";
                break;
            case CL_DEVICE_TYPE_GPU:
                std::cout << "GPU";
                break;
        }
        std::cout << std::endl;
    }
}