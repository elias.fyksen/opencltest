//
// Created by elias on 1/14/18.
//

#ifndef OPENCLTEST_OPENCL_H
#define OPENCLTEST_OPENCL_H


#include <CL/cl.h>

class OpenCL {
private:

    struct Platform{

        struct Device{
            cl_device_id id;
            cl_device_type type;
            char *name;
            char *vendor;
        };

        cl_platform_id id;
        cl_uint deviceArraySize = 0;
        Device *deviceArray;
        char *name;
        char *vendor;

        size_t currentDevice = 0;

        void initDevices();
    };

    cl_uint platformArraySize = 0;
    Platform *platformArray;

    size_t currentPlatform = 0;

    void initPlatforms();

public:

    OpenCL();
    ~OpenCL();
};


#endif //OPENCLTEST_OPENCL_H
